#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <iostream>

typedef struct elemento{
	double x, y, z, w, custo;
} elemento;

double compute (double *x, int n) {

   int i;
   double sum = 0;

   if (n < 1) return -1;

   for (i = 0;  i < n; i++) {
    sum += x[i] * x[i];
   }

   return sum;
}

double noise() {
    return rand() % 1;
}

bool compare(double a, double b) {
    return a < b;
}

void bubbleSort(elemento* vet, int length) {
	int i, j, temp;

	for (i = 0; i < length - 1; i++){
		 for (j = (i+1); j < length; j++){
		      if (vet[j].custo < vet[i].custo){
		           temp = vet[i].custo;
		           vet[i].custo = vet[j].custo;
		           vet[j].custo = temp;
		      }
		  }
  }
}
/**
 * pop - populacao
 * pop_size - tamanho da populacao
 * values - numero de atributos de uma amostra
**/
void pseudo_annealing(elemento* pop, int pop_size, int values)
{
    double aux[4];
		int iteracoes = 0;
		bubbleSort(pop, pop_size);
   
		while(iteracoes++ < 90000){

		  for(int i = 0; i < pop_size; i++)
		  {

		      pop[i].x += noise();
					pop[i].y += noise();
					pop[i].z += noise();
					pop[i].w += noise();

					aux[0] = pop[i].x;
					aux[1] = pop[i].y;
					aux[2] = pop[i].z;
					aux[3] = pop[i].w;

		      pop[ i ].custo = compute(aux, 4);

		  }

			bubbleSort(pop, pop_size);

		}

    for(int i = 0; i < pop_size; i++)
				printf("x: %.10lf y: %.10lf z: %.10lf w: %.10lf custo: %.10lf \n", pop[i].x, pop[i].y, pop[i].z, pop[i].w, pop[i].custo);

//getchar ();

}

void gera_populacao(elemento* populacao){
	
	int i = 0;
	double aux[4], val;

	for( ; i < 10; i++){
		populacao[i].x = rand() % 1;
	  populacao[i].y = rand() % 2;
		populacao[i].z = rand() % 3;
		populacao[i].w = rand() % 4;

		aux[0] = populacao[i].x;
		aux[1] = populacao[i].y;
		aux[2] = populacao[i].z;
		aux[3] = populacao[i].w;

		val = populacao[i].custo = compute(aux, 4);

  //  printf ("\n%lf", val);

	}

   // std::cout << "\n\n";

}

int main() {

    elemento populacao[10];

	//srand (TIME(NULL));

	gera_populacao(populacao);

    pseudo_annealing(populacao, 10, 5);
    
    printf("custo: %.4lf \n", populacao[0].custo);

    return 0;
    
}
